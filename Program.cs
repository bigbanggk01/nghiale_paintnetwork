﻿using AppSocket;

namespace Application
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await ServerSocket.StartListening();
        }
    }
}