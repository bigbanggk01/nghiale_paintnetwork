namespace Entities
{
    class Room
    {
        Guid roomId { get; set; }
        List<User> users { get; set; }
    }
}