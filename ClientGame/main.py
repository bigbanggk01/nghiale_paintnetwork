from tkinter import *
from tkinter.colorchooser import askcolor
import socket
import threading

class PaintApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Paint App")
        self.master.geometry("800x600")

        self.brush_size = 10
        self.color = "black"
        self.old_x = None
        self.old_y = None

        self.canvas = Canvas(self.master, bg="white")
        self.canvas.pack(fill=BOTH, expand=YES)

        self.canvas.bind("<B1-Motion>", self.paint)
        self.canvas.bind("<ButtonRelease-1>", self.reset)

        self.menu = Menu(self.master)
        self.master.config(menu=self.menu)

        self.file_menu = Menu(self.menu)
        self.menu.add_cascade(label="File", menu=self.file_menu)
        self.file_menu.add_command(label="Exit", command=self.quit)

        self.brush_menu = Menu(self.menu)
        self.menu.add_cascade(label="Brush", menu=self.brush_menu)
        self.brush_menu.add_command(label="Size", command=self.change_brush_size)
        self.brush_menu.add_command(label="Color", command=self.change_brush_color)

        self.host = "127.0.0.1"
        self.port = 8080
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

        # Start a separate thread to listen for incoming data from the server
        self.listen_thread = threading.Thread(target=self.listen)
        self.listen_thread.start()

    def paint(self, event):
        if self.old_x and self.old_y:
            self.canvas.create_line(self.old_x, self.old_y, event.x, event.y, width=self.brush_size, fill=self.color, capstyle=ROUND, smooth=TRUE)
            data = "{}|{}|{}|{}|{}|{}".format(self.old_x, self.old_y, event.x, event.y, self.brush_size, self.color)
        
            self.socket.send(data.encode())

        self.old_x = event.x
        self.old_y = event.y

    def reset(self, event):
        self.old_x, self.old_y = None, None

    def change_brush_size(self):
        self.brush_size = simpledialog.askinteger("Brush Size", "Enter brush size:", initialvalue=self.brush_size)

    def change_brush_color(self):
        color = askcolor(color=self.color)[1]
        if color:
            self.color = color

    def quit(self):
        self.socket.close()
        self.master.quit()

    def listen(self):
        while True:
            data = self.socket.recv(1024).decode()
            if data:
                draw_metric = data.split("|")
                self.canvas.create_line(int(draw_metric[0]), int(draw_metric[1]), 
                    int(draw_metric[2]), int(draw_metric[3]), width=draw_metric[4], fill=draw_metric[5], capstyle=ROUND, smooth=TRUE)

if __name__ == "__main__":
    root = Tk()
    PaintApp(root)
    root.mainloop()
