using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AppSocket {
    class ServerSocket {
        static Socket _listener;
        private static ConcurrentDictionary<int, Socket> _clients = new ConcurrentDictionary<int, Socket>();


        public static async Task StartListening() {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _listener.Bind(new IPEndPoint(IPAddress.Any, 8080));
            _listener.Listen(10);
            Console.WriteLine("Server started, listening on port 8080");
            await ListenForClients();
        }

        private static async Task ListenForClients() {
            int clientId = 0;
            while (true) {
                Socket client = await _listener.AcceptAsync();
                _clients.TryAdd(clientId, client);
                Console.WriteLine($"Client {clientId} connected");
                var currentClientId = clientId;
                Task.Run(() => HandleClientData(currentClientId));
                clientId++;
            }
        }

        private static async Task HandleClientData(int clientId)
        {
            Socket client;
            _clients.TryGetValue(clientId, out client);
            try
            {
                while (true)
                {
                    if (client?.Connected == true)
                    {
                        byte[] buffer = new byte[1024];
                        int bytesRead = await client.ReceiveAsync(new ArraySegment<byte>(buffer), SocketFlags.None);
                        string data = Encoding.ASCII.GetString(buffer, 0, bytesRead);

                        Console.WriteLine($"Received data from client {clientId}: {data}");
                        foreach (var pair in _clients)
                        {
                            int otherClientId = pair.Key;
                            Socket socket = pair.Value;
                            if (socket != client)
                            {
                                byte[] dataBytes = Encoding.ASCII.GetBytes(data);
                                await socket.SendAsync(new ArraySegment<byte>(dataBytes), SocketFlags.None);
                                Console.WriteLine($"Sent data to client {otherClientId}: {data}");
                            }
                        }
                    }
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine($"Client {clientId} disconnected: {ex.Message}");
                _clients.TryRemove(clientId, out client);
                client.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error handling client {clientId}: {ex.Message}");
            }
        }
    }
}